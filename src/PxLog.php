<?php
namespace  CqPx;
class PxLog
{
    private $base_url="http://log.pangxie.wkh01.top:8085";
    private $app_name="";
    private $timeout=30;
    function __construct($app_name,$host="http://log.pangxie.wkh01.top:8085")
    {
        if(!preg_match('/^[_a-zA-Z\-\d]+$/', $app_name)){
            throw new \Exception("app_name 只能包含英文字符和数字-_");
        }
        $this->app_name=trim($app_name);
        $this->base_url=$host;
    }

    function verify_base_parms() : bool {
        if(empty($this->app_name))
            throw new \Exception("app_name is not null");
        return true;
    }

    function msg($level,$msg) : array {
        $this->verify_base_parms();
        if(is_object($msg)) {
            $msg = get_object_vars($msg);
        }
        if(is_array($msg)){
            $msg=json_encode($msg);
        }
        $data=array(
            "app"=>$this->app_name,
            "level"=>$level,
            "msg"=>$msg
        );

        $url=$this->base_url."/log";
        try {
            $rest_str= $this->_post_json($url,$data);
            $rest=json_decode($rest_str,true);
            if ($rest==null)
                return array();
            return $rest;
        } catch (\Exception $ex) {
            //throw $th;
            //throw $ex;
        }
        return array();
    }
    function info($msg) : array {
        return $this->msg("info",$msg);
    }
    function debug($msg) : array {
        return $this->msg("debug",$msg);
    }
    function waring($msg) : array {
        return $this->msg("waring",$msg);
    }
    function error($msg) : array {
        return $this->msg("error",$msg);
    }
    function get_log_url() : string {
        return $this->base_url."/show_log/".$this->app_name;
    }

    function _get($url,$headers=null) : string {

        if($headers==null){
            $headers=array();
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => $headers,
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    function _post_form($url,$data=null,$headers=null) : string {

        if($headers==null){
            $headers=array();
        }
        if($data==null){
            $data=array();
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => $data,
        CURLOPT_HTTPHEADER => $headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }

    function _post_json($url,$data=null,$headers=null) : string {
        if($headers==null){
            $headers=array(
                'Content-Type: application/json'
            );
        }else{
            $headers[]='Content-Type: application/json';
        }
        if($data==null){
            $data=array();
        }
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => $this->timeout,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS =>json_encode($data),
        CURLOPT_HTTPHEADER =>$headers,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }
}
?>